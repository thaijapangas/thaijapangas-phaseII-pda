﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using UserInterfaces.Configs;
using UserInterfaces.Execute;
using UI.Configs;
using UserInterfaces.Activates;

namespace UserInterfaces
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main()
        {
            if (Executing.Instance.CheckExpireSoftware().Trim() == "TRUE")
            {
                Application.Run(new Main.frmMainMenu("TRUE"));
            }
            else if (Executing.Instance.CheckExpireSoftware().Trim() == "LIFETIME")
            {
                Application.Run(new Main.frmMainMenu("LIFETIME"));
            }
            else if (Executing.Instance.CheckExpireSoftware().Trim() == "EXPIRE")
            {
                if (MessageActivate.DialogQuestion("โปรแกรมหมดอายุการใช้งาน กรุณาติดต่อผู้ดูแลระบบเพื่อทำการลงทะเบียนโปรแกรม?")
                        == System.Windows.Forms.DialogResult.Yes)
                {
                    using (frmActivateLicense fAvtivate = new frmActivateLicense())
                    {
                        fAvtivate.ShowDialog();
                    }
                }
                else
                {
                    Application.Exit();
                }
            }
            else if (Executing.Instance.CheckExpireSoftware().Trim() == "FALSE")
            {
                MsgBox.ClassMsgBox.ClassMsg.DialogWarning("หมายเลขลงทะเบียนไม่ถูกต้องไม่สามารถเปิดโปรแกรมได้ กรุณาติดต่อ IT หรือผู้เกี่ยวข้องกับระบบ");
                Application.Exit();
            }
        }
    }
}