﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using EntitiesTemp.GIDelivery;
using EntitiesTemp.GRProduction;
using EntitiesTemp.GIRefIOEmpty;
using EntitiesTemp.Customer;
using EntitiesTemp.DistributionRet;
using System.Globalization;
using ServiceFunction.Function;

namespace UserInterfaces.Execute
{
    public class Executing : Connections.Connection, IDisposable
    {
        #region I. Instance
        public static Executing Instance
        {
            get
            {
                Executing instance = new Executing();
                return instance;
            }
        }
        #endregion

        #region 0. getDateTime
        //Get DateTime
        public DateTime GetDateServer()
        {
            try
            {
                DateTime dtpDateTime = DateTime.Now;

                string sql = "select GETDATE() AS getDateTime";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();

                DataTable dtDateTime = (DataTable)dbManager.ExecuteToDataTable(command);

                return (DateTime)dtDateTime.Rows[0]["getDateTime"];
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region 1. GI Ref. Delivery

        //Check Serial Number
        public bool checkGIDeliverySerialDuplicate(string _serial_number)
        {
            try
            {
                DataTable dt_chk_serial = new DataTable();
                string sql = "SELECT * " +
                             "FROM GI_Delivery " +
                             "WHERE (serial_number='" + _serial_number + "') ";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_chk_serial = dbManager.ExecuteToDataTable(command);

                if (dt_chk_serial.Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool SaveGIDelivery(GIDeliveryEntity _ent)
        {
            bool _result = false;
            try
            {

                StringBuilder sql = new StringBuilder();
                sql.Append("INSERT INTO GI_Delivery (serial_number, do_no, create_date) ");
                sql.Append(string.Format("VALUES ('{0}','{1}','{2}') ", _ent.serial_number, _ent.do_no, _ent.create_date));

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();

                dbManager.Open();
                if (dbManager.ExecuteNonQuery(command) > 0)
                    _result = true;
            }
            catch (SqlCeException)
            {
                _result = false;
                throw;
            }
            finally
            {
                if (dbManager != null)
                {
                    dbManager.Dispose();
                }
            }
            return _result;
        }
        public DataTable getGIDeliveryForDel()
        {
            try
            {
                DataTable dt_get = new DataTable();

                string sql = "SELECT * " +
                             "FROM GI_Delivery " +
                             "ORDER BY create_date DESC";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_get = dbManager.ExecuteToDataTable(command);

                return dt_get;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public string CountGIALL()
        {
            string value = string.Empty;
            try
            {
                DataTable dt_get = new DataTable();
                string _count = string.Empty;

                string sql = "SELECT COUNT(*) AS count_gi_row " +
                             "FROM GI_Delivery ";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_get = dbManager.ExecuteToDataTable(command);

                if (dt_get.Rows.Count > 0)
                    value = Convert.ToString(dt_get.Rows[0]["count_gi_row"]);

                return value;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public string CountGIByDO(string _do_no)
        {
            string value = string.Empty;
            try
            {
                DataTable dt_get = new DataTable();
                string _count = string.Empty;

                string sql = "SELECT COUNT(*) AS count_gi_by_do_no " +
                             "FROM GI_Delivery " +
                             "WHERE (do_no='" + _do_no + "') ";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_get = dbManager.ExecuteToDataTable(command);

                if (dt_get.Rows.Count > 0)
                    value = Convert.ToString(dt_get.Rows[0]["count_gi_by_do_no"]);
                else
                    value = "0";
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (this.dbManager != null)
                {
                    this.dbManager.Dispose();
                }
            }

            return value;
        }
        public DataTable getInvoiceDO(string _do_no)
        {
            DataTable result;
            try
            {
                DataTable dataTable = new DataTable();
                string text = "SELECT create_date, do_no, serial_number FROM GI_Delivery WHERE (do_no='" + _do_no + "') ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = text.ToString();
                this.dbManager.Open();
                dataTable = this.dbManager.ExecuteToDataTable(sqlCeCommand);
                this.dbManager.Close();
                result = dataTable;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (this.dbManager != null)
                {
                    this.dbManager.Dispose();
                }
            }
            return result;
        }

        #endregion

        #region 2. GR From Production

        public bool checkGRProductionSerialDuplicate(string _serial_number)
        {
            try
            {
                DataTable dt_chk_serial = new DataTable();
                string sql = "SELECT * " +
                             "FROM GR_Production " +
                             "WHERE (serial_number='" + _serial_number + "') ";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_chk_serial = dbManager.ExecuteToDataTable(command);

                if (dt_chk_serial.Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool SaveGRProduction(GRProductionEntity _ent)
        {
            bool _result = false;
            try
            {

                StringBuilder sql = new StringBuilder();
                sql.Append("INSERT INTO GR_Production (serial_number, pre_order, batch, create_date) ");
                sql.Append(string.Format("VALUES ('{0}','{1}','{2}','{3}') ", _ent.serial_number, _ent.pre_order, _ent.batch, _ent.create_date));

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();

                dbManager.Open();
                if (dbManager.ExecuteNonQuery(command) > 0)
                    _result = true;
            }
            catch (SqlCeException)
            {
                _result = false;
                throw;
            }
            finally
            {
                if (dbManager != null)
                {
                    dbManager.Dispose();
                }
            }
            return _result;
        }
        public string CountGRProductionALL()
        {
            string value = string.Empty;
            try
            {
                DataTable dt_get = new DataTable();
                string _count = string.Empty;

                string sql = "SELECT COUNT(*) AS count_gr_row " +
                             "FROM GR_Production ";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_get = dbManager.ExecuteToDataTable(command);

                if (dt_get.Rows.Count > 0)
                    value = Convert.ToString(dt_get.Rows[0]["count_gr_row"]);

                return value;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public string CountByPreOrderGRProduction(string _pre_order)
        {
            string value = string.Empty;
            try
            {
                DataTable dt_get = new DataTable();
                string _count = string.Empty;

                string sql = "SELECT COUNT(*) AS count_by_pre_order " +
                             "FROM GR_Production " +
                             "WHERE (pre_order = '" + _pre_order + "') ";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_get = dbManager.ExecuteToDataTable(command);

                if (dt_get.Rows.Count > 0)
                    value = Convert.ToString(dt_get.Rows[0]["count_by_pre_order"]);

                return value;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region 3. Distribution Ret

        public bool checkCustCode(string _cust_code, string _cust_id, ref string _error_code, ref string _cust_name)
        {
            bool result;
            try
            {
                DataTable dataTable = new DataTable();
                DataTable dataTable2 = new DataTable();
                string sql = "SELECT * FROM customer WHERE (SUBSTRING(cust_id, 1, 3)  = '" + _cust_code + "') ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = sql.ToString();
                this.dbManager.Open();
                dataTable = this.dbManager.ExecuteToDataTable(sqlCeCommand);
                this.dbManager.Close();
                bool flag;
                if (dataTable.Rows.Count > 0)
                {
                    string sql2 = "SELECT * FROM customer WHERE (cust_id  = '" + _cust_id + "') ";
                    SqlCeCommand sqlCeCommand2 = new SqlCeCommand();
                    sqlCeCommand2.CommandType = CommandType.Text;
                    sqlCeCommand2.CommandText = sql2.ToString();
                    this.dbManager.Open();
                    dataTable2 = this.dbManager.ExecuteToDataTable(sqlCeCommand2);
                    if (dataTable2.Rows.Count > 0)
                    {
                        flag = true;
                        _cust_name = dataTable2.Rows[0]["cust_name"].ToString().Trim();
                    }
                    else
                    {
                        flag = false;
                        _error_code = "002";
                    }
                }
                else
                {
                    flag = false;
                    _error_code = "001";
                }
                result = flag;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (this.dbManager != null)
                {
                    this.dbManager.Dispose();
                }
            }
            return result;
        }
        public string CountDistributionRetALL()
        {
            string text = string.Empty;
            string result;
            try
            {
                DataTable dataTable = new DataTable();
                string empty = string.Empty;
                string sql = "SELECT COUNT(*) AS count_dis_ret FROM Distribution_Ret ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = sql.ToString();
                this.dbManager.Open();
                dataTable = this.dbManager.ExecuteToDataTable(sqlCeCommand);
                this.dbManager.Close();
                if (dataTable.Rows.Count > 0)
                    text = Convert.ToString(dataTable.Rows[0]["count_dis_ret"]);
                else
                    text = "0";

                result = text;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (this.dbManager != null)
                {
                    this.dbManager.Dispose();
                }
            }
            return result;
        }
        public DataTable getDetailCustomer(string _cust_code, string _doc_number, string _vehicle)
        {
            DataTable result;
            try
            {
                DataTable dataTable = new DataTable();
                string sql = "SELECT create_date, empty_or_full, cust_id, doc_no, serial_number, vehicle FROM Distribution_Ret WHERE (cust_id ='" + _cust_code + "') AND (doc_no ='" + _doc_number + "') AND (vehicle ='" + _vehicle + "') ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = sql.ToString();
                this.dbManager.Open();
                dataTable = this.dbManager.ExecuteToDataTable(sqlCeCommand);
                this.dbManager.Close();
                result = dataTable;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (this.dbManager != null)
                {
                    this.dbManager.Dispose();
                }
            }
            return result;
        }
        public bool checkDistributionRetSerialDuplicate(string _serial_number)
        {
            bool result;
            try
            {
                DataTable dataTable = new DataTable();
                string sql = "SELECT * FROM Distribution_Ret WHERE (serial_number='" + _serial_number + "') ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = sql.ToString();
                this.dbManager.Open();
                dataTable = this.dbManager.ExecuteToDataTable(sqlCeCommand);
                this.dbManager.Close();

                if (dataTable.Rows.Count > 0)
                    result = true;
                else
                    result = false;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (this.dbManager != null)
                {
                    this.dbManager.Dispose();
                }
            }
            return result;
        }
        public bool SaveDistributionRet(DistributionRetEntity _ent)
        {
            bool result = false;
            try
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("INSERT INTO Distribution_Ret (serial_number, cust_id, doc_no, vehicle, empty_or_full, create_date) ");
                stringBuilder.Append(string.Format("VALUES ('{0}','{1}','{2}','{3}','{4}','{5}') ", _ent.serial_number, _ent.cust_id, _ent.doc_no, _ent.vehicle, _ent.empty_or_full, _ent.create_date));
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = stringBuilder.ToString();
                this.dbManager.Open();

                if (this.dbManager.ExecuteNonQuery(sqlCeCommand) > 0)
                    result = true;

                this.dbManager.Close();
            }
            catch (SqlCeException)
            {
                result = false;
                throw;
            }
            finally
            {
                if (this.dbManager != null)
                {
                    this.dbManager.Dispose();
                }
            }
            return result;
        }
        public string DistributionRetByCustomer(string _cust_id)
        {
            string text = string.Empty;
            string result;
            try
            {
                DataTable dataTable = new DataTable();
                string empty = string.Empty;
                string sql = "SELECT COUNT(*) AS count_dis_ret FROM Distribution_Ret WHERE (cust_id = '" + _cust_id + "') ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = sql.ToString();
                this.dbManager.Open();
                dataTable = this.dbManager.ExecuteToDataTable(sqlCeCommand);
                this.dbManager.Close();
                if (dataTable.Rows.Count > 0)
                {
                    text = Convert.ToString(dataTable.Rows[0]["count_dis_ret"]);
                }

                result = text;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (this.dbManager != null)
                {
                    this.dbManager.Dispose();
                }
            }
            return result;
        }
        public DataTable getDistributionRetForDel()
        {
            DataTable result;
            try
            {
                DataTable dataTable = new DataTable();
                string sql = "SELECT * FROM Distribution_Ret ORDER BY create_date DESC";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = sql.ToString();
                this.dbManager.Open();
                dataTable = this.dbManager.ExecuteToDataTable(sqlCeCommand);
                this.dbManager.Close();
                result = dataTable;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (this.dbManager != null)
                {
                    this.dbManager.Dispose();
                }
            }
            return result;
        }
        public bool DeleteGIDelivery(string _do_no, string _serial_number)
        {
            bool result;
            try
            {
                string sql = string.Concat(new string[]
		{
			"DELETE FROM GI_Delivery WHERE (do_no = '",
			_do_no,
			"') AND (serial_number = '",
			_serial_number,
			"')"
		});
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = sql.ToString();
                this.dbManager.Open();
                this.dbManager.ExecuteNonQuery(sqlCeCommand);
                this.dbManager.Close();
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            finally
            {
                if (this.dbManager != null)
                {
                    this.dbManager.Dispose();
                }
            }
            return result;
        }
        public bool DeleteDistributionRet(string _cust_id, string _doc_no, string _vehicle, string _empty_or_full, string _serial_number)
        {
            bool result;
            try
            {
                string sql = string.Concat(new string[]
		{
			"DELETE FROM Distribution_Ret WHERE (cust_id = '",
			_cust_id,
			"') AND (doc_no = '",
			_doc_no,
			"') AND (vehicle = '",
			_vehicle,
			"') AND (empty_or_full = '",
			_empty_or_full,
			"') AND (serial_number = '",
			_serial_number,
			"')"
		});
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = sql.ToString();
                this.dbManager.Open();
                this.dbManager.ExecuteNonQuery(sqlCeCommand);
                this.dbManager.Close();
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            finally
            {
                if (this.dbManager != null)
                {
                    this.dbManager.Dispose();
                }
            }
            return result;
        }
        public bool DeleteDistributionRet()
        {
            bool result;
            try
            {
                string text = "DELETE FROM Distribution_Ret ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = text.ToString();
                this.dbManager.Open();
                this.dbManager.ExecuteNonQuery(sqlCeCommand);
                this.dbManager.Close();
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            finally
            {
                if (this.dbManager != null)
                {
                    this.dbManager.Dispose();
                }
            }
            return result;
        }
        public bool DeleteDistributionSale()
        {
            bool result;
            try
            {
                string text = "DELETE FROM GI_Delivery ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = text.ToString();
                this.dbManager.Open();
                this.dbManager.ExecuteNonQuery(sqlCeCommand);
                this.dbManager.Close();
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            finally
            {
                if (this.dbManager != null)
                {
                    this.dbManager.Dispose();
                }
            }
            return result;
        }
        public string CountRowDeleteDistributionRetByCustCode(string _cust_code)
        {
            string text = string.Empty;
            string result;
            try
            {
                DataTable dataTable = new DataTable();
                string empty = string.Empty;
                string text2 = "SELECT COUNT(*) AS count_dis_by_cust_code FROM Distribution_Ret WHERE (cust_id = '" + _cust_code + "') ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = text2.ToString();
                this.dbManager.Open();
                dataTable = this.dbManager.ExecuteToDataTable(sqlCeCommand);
                this.dbManager.Close();
                if (dataTable.Rows.Count > 0)
                    text = Convert.ToString(dataTable.Rows[0]["count_dis_by_cust_code"]);
                else
                    text = "0";
                result = text;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (this.dbManager != null)
                {
                    this.dbManager.Dispose();
                }
            }
            return result;
        }
        public bool DeleteDistributionRetByCustCode(string _cust_code)
        {
            bool result;
            try
            {
                string text = "DELETE FROM Distribution_Ret WHERE (cust_id = '" + _cust_code + "') ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = text.ToString();
                this.dbManager.Open();
                this.dbManager.ExecuteNonQuery(sqlCeCommand);
                this.dbManager.Close();
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            finally
            {
                if (this.dbManager != null)
                {
                    this.dbManager.Dispose();
                }
            }
            return result;
        }
        public string CountRowDeleteGIByDO(string _do_no)
        {
            string text = string.Empty;
            string result;
            try
            {
                DataTable dataTable = new DataTable();
                string empty = string.Empty;
                string sql = "SELECT COUNT(*) AS count_gi_by_do_no FROM GI_Delivery WHERE (do_no='" + _do_no + "') ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = sql.ToString();
                this.dbManager.Open();
                dataTable = this.dbManager.ExecuteToDataTable(sqlCeCommand);
                this.dbManager.Close();

                if (dataTable.Rows.Count > 0)
                    text = Convert.ToString(dataTable.Rows[0]["count_gi_by_do_no"]);
                else
                    text = "0";

                result = text;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (this.dbManager != null)
                {
                    this.dbManager.Dispose();
                }
            }
            return result;
        }
        public bool DeleteGIDeliveryByDO(string _do_no)
        {
            bool result;
            try
            {
                string text = "DELETE FROM GI_Delivery WHERE (do_no = '" + _do_no + "') ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = text.ToString();
                this.dbManager.Open();
                this.dbManager.ExecuteNonQuery(sqlCeCommand);
                this.dbManager.Close();
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            finally
            {
                if (this.dbManager != null)
                {
                    this.dbManager.Dispose();
                }
            }
            return result;
        }

        #endregion

        #region 4. GI Ref. IO Empty

        public bool checkGIRefIOEmptySerialDuplicate(string _serial_number)
        {
            try
            {
                DataTable dt_chk_serial = new DataTable();
                string sql = "SELECT * " +
                             "FROM GI_Ref_IO_Empty " +
                             "WHERE (serial_number='" + _serial_number + "') ";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_chk_serial = dbManager.ExecuteToDataTable(command);

                if (dt_chk_serial.Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public string CountGIRefIOALL()
        {
            string value = string.Empty;
            try
            {
                DataTable dt_get = new DataTable();
                string _count = string.Empty;

                string sql = "SELECT COUNT(*) AS count_gi_io_empty " +
                             "FROM GI_Ref_IO_Empty ";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_get = dbManager.ExecuteToDataTable(command);

                if (dt_get.Rows.Count > 0)
                    value = Convert.ToString(dt_get.Rows[0]["count_gi_io_empty"]);

                return value;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public string CountGIEmptyByDO(string _do_no)
        {
            string text = string.Empty;
            string result;
            try
            {
                DataTable dataTable = new DataTable();
                string empty = string.Empty;
                string text2 = "SELECT COUNT(*) AS count_gi_by_do_no FROM GI_Ref_IO_Empty WHERE (do_no = '" + _do_no + "') ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = text2.ToString();
                this.dbManager.Open();
                dataTable = this.dbManager.ExecuteToDataTable(sqlCeCommand);
                this.dbManager.Close();
                if (dataTable.Rows.Count > 0)
                {
                    text = Convert.ToString(dataTable.Rows[0]["count_gi_by_do_no"]);
                }
                result = text;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (this.dbManager != null)
                {
                    this.dbManager.Dispose();
                }
            }
            return result;
        }
        public bool SaveGIRefIOEmpty(GIRefIOEmptyEntity _ent)
        {
            bool _result = false;
            try
            {

                StringBuilder sql = new StringBuilder();
                sql.Append("INSERT INTO GI_Ref_IO_Empty (serial_number, do_no, create_date) ");
                sql.Append(string.Format("VALUES ('{0}','{1}','{2}') ", _ent.serial_number, _ent.do_no, _ent.create_date));

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();

                dbManager.Open();
                if (dbManager.ExecuteNonQuery(command) > 0)
                    _result = true;
            }
            catch (SqlCeException)
            {
                _result = false;
                throw;
            }
            finally
            {
                if (dbManager != null)
                {
                    dbManager.Dispose();
                }
            }
            return _result;
        }

        #endregion

        #region 5. Delete All Data

        public bool DeleteAllInSqlCeOnPDA()
        {
            StringBuilder stringBuilder = new StringBuilder();
            StringBuilder stringBuilder2 = new StringBuilder();
            StringBuilder stringBuilder3 = new StringBuilder();
            StringBuilder stringBuilder4 = new StringBuilder();
            SqlCeCommand sqlCeCommand = new SqlCeCommand();
            SqlCeCommand sqlCeCommand2 = new SqlCeCommand();
            SqlCeCommand sqlCeCommand3 = new SqlCeCommand();
            SqlCeCommand sqlCeCommand4 = new SqlCeCommand();
            bool result;
            try
            {
                this.dbManager.Open();
                this.dbManager.BeginTransaction();
                stringBuilder = new StringBuilder();
                stringBuilder.Append(string.Format("DELETE FROM Distribution_Ret ", new object[0]));
                sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = stringBuilder.ToString();
                bool flag;
                if (this.dbManager.ExecuteNonQuery(sqlCeCommand) >= 0)
                {
                    stringBuilder2.Append(string.Format("DELETE FROM GI_Delivery ", new object[0]));
                    sqlCeCommand2 = new SqlCeCommand();
                    sqlCeCommand2.CommandType = CommandType.Text;
                    sqlCeCommand2.CommandText = stringBuilder2.ToString();
                    if (this.dbManager.ExecuteNonQuery(sqlCeCommand2) >= 0)
                    {
                        stringBuilder3.Append(string.Format("DELETE FROM GI_Ref_IO_Empty ", new object[0]));
                        sqlCeCommand3 = new SqlCeCommand();
                        sqlCeCommand3.CommandType = CommandType.Text;
                        sqlCeCommand3.CommandText = stringBuilder3.ToString();
                        if (this.dbManager.ExecuteNonQuery(sqlCeCommand3) >= 0)
                        {
                            stringBuilder4.Append(string.Format("DELETE FROM GR_Production ", new object[0]));
                            sqlCeCommand4 = new SqlCeCommand();
                            sqlCeCommand4.CommandType = CommandType.Text;
                            sqlCeCommand4.CommandText = stringBuilder4.ToString();
                            if (this.dbManager.ExecuteNonQuery(sqlCeCommand4) >= 0)
                            {
                                this.dbManager.CommitTransaction();
                                flag = true;
                                this.dbManager.Close();
                            }
                            else
                            {
                                flag = false;
                            }
                        }
                        else
                        {
                            flag = false;
                        }
                    }
                    else
                    {
                        flag = false;
                    }
                }
                else
                {
                    flag = false;
                }
                result = flag;
            }
            catch (Exception)
            {
                this.dbManager.RollBackTransaction();
                throw;
            }
            finally
            {
                if (this.dbManager != null)
                {
                    this.dbManager.Dispose();
                }
            }
            return result;
        }
        public bool DeleteGIRefIOEmpty()
        {
            bool result;
            try
            {
                string text = "DELETE FROM GI_Ref_IO_Empty ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = text.ToString();
                this.dbManager.Open();
                this.dbManager.ExecuteNonQuery(sqlCeCommand);
                this.dbManager.Close();
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            finally
            {
                if (this.dbManager != null)
                {
                    this.dbManager.Dispose();
                }
            }
            return result;
        }
        public bool DeleteGRFromProduction()
        {
            bool result;
            try
            {
                string text = "DELETE FROM GR_Production ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = text.ToString();
                this.dbManager.Open();
                this.dbManager.ExecuteNonQuery(sqlCeCommand);
                this.dbManager.Close();
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            finally
            {
                if (this.dbManager != null)
                {
                    this.dbManager.Dispose();
                }
            }
            return result;
        }

        #endregion

        #region 6. Download Customer

        public bool getImportCustomer()
        {
            bool result;
            try
            {
                DataTable dataTable = new DataTable();
                string sql = "SELECT * FROM customer ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = sql.ToString();
                this.dbManager.Open();
                dataTable = this.dbManager.ExecuteToDataTable(sqlCeCommand);
                this.dbManager.Close();

                if (dataTable.Rows.Count > 0)
                    result = true;
                else
                    result = false;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (this.dbManager != null)
                {
                    this.dbManager.Dispose();
                }
            }
            return result;
        }
        public bool DeleteCustomerInSqlCeOnPDA()
        {
            bool result;
            try
            {
                string sql = "DELETE FROM customer ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = sql.ToString();
                this.dbManager.Open();
                bool flag;
                if (this.dbManager.ExecuteNonQuery(sqlCeCommand) > 0)
                {
                    flag = true;
                    this.dbManager.Close();
                }
                else
                {
                    flag = false;
                }
                result = flag;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (this.dbManager != null)
                {
                    this.dbManager.Dispose();
                }
            }
            return result;
        }
        public bool SaveImportCustomer(List<CustomerEntity> _list)
        {
            StringBuilder sql = new StringBuilder();
            SqlCeCommand command = new SqlCeCommand();
            bool _result = false;
            try
            {
                foreach (var _ent in _list)
                {
                    string _cust_name = _ent.cust_name.Replace("'", "''");
                    sql = new StringBuilder();
                    sql.Append("INSERT INTO customer (cust_id, cust_name) ");
                    sql.Append(string.Format("VALUES ('{0}','{1}') ", _ent.cust_id, _cust_name));

                    command = new SqlCeCommand();
                    command.CommandType = CommandType.Text;
                    command.CommandText = sql.ToString();

                    dbManager.Open();
                    if (dbManager.ExecuteNonQuery(command) > 0)
                        _result = true;
                    else
                        _result = false;
                }

                this.dbManager.Close();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (this.dbManager != null)
                {
                    this.dbManager.Dispose();
                }
            }
            return _result;
        }

        #endregion

        #region 7. Upload Data

        public DataTable getDataDistributionRet()
        {
            try
            {
                DataTable dt_get = new DataTable();

                string sql = "SELECT * " +
                             "FROM Distribution_Ret ";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_get = dbManager.ExecuteToDataTable(command);

                return dt_get;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable getDataGIRefIOEmpty()
        {
            try
            {
                DataTable dt_get = new DataTable();

                string sql = "SELECT * " +
                             "FROM GI_Ref_IO_Empty ";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_get = dbManager.ExecuteToDataTable(command);

                return dt_get;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable getDataGIDelivery()
        {
            try
            {
                DataTable dt_get = new DataTable();

                string sql = "SELECT * " +
                             "FROM GI_Delivery ";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_get = dbManager.ExecuteToDataTable(command);

                return dt_get;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable getDataGRProduction()
        {
            try
            {
                DataTable dt_get = new DataTable();

                string sql = "SELECT * " +
                             "FROM GR_Production ";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_get = dbManager.ExecuteToDataTable(command);

                return dt_get;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool DeleteDistributionRetAfterUpload()
        {
            bool _result = false;
            try
            {
                string text = "DELETE FROM Distribution_Ret ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = text.ToString();
                this.dbManager.Open();
                this.dbManager.ExecuteNonQuery(sqlCeCommand);
                this.dbManager.Close();
                _result = true;
            }
            catch (SqlCeException)
            {
                _result = false;
                throw;
            }
            finally
            {
                if (dbManager != null)
                {
                    dbManager.Dispose();
                }
            }
            return _result;
        }
        public bool DeleteGIRefIOEmptyAfterUpload()
        {
            bool _result = false;
            try
            {
                string text = "DELETE FROM GI_Ref_IO_Empty ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = text.ToString();
                this.dbManager.Open();
                this.dbManager.ExecuteNonQuery(sqlCeCommand);
                this.dbManager.Close();
                _result = true;
            }
            catch (SqlCeException)
            {
                _result = false;
                throw;
            }
            finally
            {
                if (dbManager != null)
                {
                    dbManager.Dispose();
                }
            }
            return _result;
        }
        public bool DeleteGIDeliveryAfterUpload()
        {
            bool _result = false;
            try
            {
                string text = "DELETE FROM GI_Delivery ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = text.ToString();
                this.dbManager.Open();
                this.dbManager.ExecuteNonQuery(sqlCeCommand);
                this.dbManager.Close();
                _result = true;
            }
            catch (SqlCeException)
            {
                _result = false;
                throw;
            }
            finally
            {
                if (dbManager != null)
                {
                    dbManager.Dispose();
                }
            }
            return _result;
        }
        public bool DeleteGRProductionAfterUpload()
        {
            bool _result = false;
            try
            {
                string text = "DELETE FROM GR_Production ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = text.ToString();
                this.dbManager.Open();
                this.dbManager.ExecuteNonQuery(sqlCeCommand);
                this.dbManager.Close();
                _result = true;
            }
            catch (SqlCeException)
            {
                _result = false;
                throw;
            }
            finally
            {
                if (dbManager != null)
                {
                    dbManager.Dispose();
                }
            }
            return _result;
        }

        #endregion

        #region 8. Insert Log

        public bool Insert_Log(string error_code, string error_message, string page_form, string method_name)
        {
            bool _result = false;
            try
            {

                StringBuilder sql = new StringBuilder();
                sql.Append("INSERT INTO Action_Log (id, error_code, error_message, page_form, method_name, create_date)  ");
                sql.Append(string.Format("VALUES ('{0}','{1}','{2}', '{3}', '{4}', getdate()) ", Guid.NewGuid().ToString().ToUpper().Trim(), error_code, error_message, page_form, method_name));

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();

                dbManager.Open();
                if (dbManager.ExecuteNonQuery(command) > 0)
                    _result = true;
            }
            catch (SqlCeException)
            {
                _result = false;
                throw;
            }
            finally
            {
                if (dbManager != null)
                {
                    dbManager.Dispose();
                }
            }
            return _result;
        }

        #endregion

        #region 9. Activate Licence

        public string CheckExpireSoftware()
        {
            string connti = "FALSE";
            try
            {
                DateTimeFormatInfo usDtfi = new CultureInfo("en-US", false).DateTimeFormat;

                DataTable dt_licence = new DataTable();

                string sql = "SELECT TOP(1) expire_date " +
                             "FROM t_setup ";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_licence = dbManager.ExecuteToDataTable(command);

                if (!dt_licence.IsNullOrNoRows())
                {
                    #region Has data

                    string _expire_date = dt_licence.Rows[0][0].ToString().Trim();
                    if (string.IsNullOrEmpty(_expire_date))
                    {
                        connti = "FALSE";
                    }
                    else if (!string.IsNullOrEmpty(_expire_date))
                    {
                        string _decry_expire_date = Convert.ToString(Convert.ToInt64(_expire_date) - 39335472);
                        if (_decry_expire_date.Trim() == "30000101")
                        {
                            connti = "LIFETIME";
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(_decry_expire_date))
                            {
                                string _crrent_date = Convert.ToDateTime(GetDateServer()).ToString("yyyyMMdd", usDtfi);

                                if (Convert.ToInt64(_decry_expire_date) < Convert.ToInt64(_crrent_date))
                                    connti = "EXPIRE"; //connti = "FALSE";
                                else if (Convert.ToInt64(_decry_expire_date) > Convert.ToInt64(_crrent_date))
                                    connti = "TRUE";
                                else
                                    connti = "FALSE";
                            }
                            else
                            {
                                connti = "FALSE";
                            }
                        }
                    }

                    #endregion
                }
                else
                {
                    #region Not has data

                    connti = "FALSE";

                    #endregion
                }
            }
            catch (Exception)
            {
                connti = "FALSE";
            }

            return connti;
        }
        public bool ActivateLicence(string _licence_key)
        {
            bool result = false;
            try
            {
                string text = "UPDATE t_setup SET expire_date = '" + _licence_key.Trim() + "', update_date = GETDATE() ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = text.ToString();
                this.dbManager.Open();
                this.dbManager.ExecuteNonQuery(sqlCeCommand);
                this.dbManager.Close();
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }
        public string ValidationLicence(string _licence_keys)
        {
            string Result = string.Empty;
            try
            {
                DateTimeFormatInfo usDtfi = new CultureInfo("en-US", false).DateTimeFormat;
                string _licence_key = Convert.ToString((Convert.ToInt64(_licence_keys) - 39335472));
                string _crrent_date = Convert.ToDateTime(GetDateServer()).ToString("yyyyMMdd", usDtfi);
                if (!string.IsNullOrEmpty(_licence_key))
                {
                    if (_licence_key.Trim() == "30000101")
                    {
                        if (ActivateLicence(_licence_keys))
                        {
                            Result = "TRUE";
                        }
                    }
                    else if (_licence_key.IndexOf("2016") >= 0)
                    {
                        if (ActivateLicence(_licence_keys))
                        {
                            DateTime dTime = DateTime.ParseExact(_licence_key, "yyyyMMdd", null);
                            string display_date = dTime.ToLongDateString();
                            Result = "TRUE_EXPRIE|" + display_date;
                        }
                    }
                    else if (Convert.ToInt32(_licence_key) < Convert.ToInt32(_crrent_date))
                    {
                        Result = "TRUE_EXPIRED";
                    }
                    else
                    {
                        Result = "FALSE";
                    }
                }
            }
            catch (Exception)
            {
                Result = "FALSE";
            }

            return Result;
        }

        #endregion

        #region G. IDisposable Members

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}