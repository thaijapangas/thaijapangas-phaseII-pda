﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Utilities.ClassMsgBox;
using UI.Configs;
using UserInterfaces.Execute;

namespace UserInterfaces.Activates
{
    public partial class frmActivateLicense : Form
    {
        #region Constuctor

        public frmActivateLicense()
        {
            InitializeComponent();
        }

        #endregion

        #region Method

        private void SubmitLicence()
        {
            if (string.IsNullOrEmpty(this.txtActivateKeys.Text.Trim()))
            {
                ClassMsg.DialogWarning("กรุณากรอกหมายเลขลงทะเบียนโปรแกรม");
                this.txtActivateKeys.Focus();
                this.txtActivateKeys.SelectAll();
                return;
            }
            else if (!string.IsNullOrEmpty(this.txtActivateKeys.Text.Trim()))
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    if (Executing.Instance.ValidationLicence(this.txtActivateKeys.Text.Trim()) == "TRUE")
                    {
                        ClassMsg.DialogInfomation("หมายเลขลงทะเบียนถูกต้อง กรุณาเปิดโปรแกรมใหม่อีกครั้ง");
                        Application.Exit();
                        //Application.Restart();
                    }
                    else if (Executing.Instance.ValidationLicence(this.txtActivateKeys.Text.Trim()).IndexOf("|") >= 0)
                    {
                        string[] array = Executing.Instance.ValidationLicence(this.txtActivateKeys.Text.Trim()).Split('|');
                        ClassMsg.DialogInfomation("หมายเลขลงทะเบียนถูกต้อง จะมีอายุการใช้งานได้ถึงวันที่ " + array[1].ToString().Trim() + " กรุณาเปิดโปรแกรมใหม่อีกครั้ง");
                        Application.Exit();
                        //Application.Restart();
                    }
                    else if (Executing.Instance.ValidationLicence(this.txtActivateKeys.Text.Trim()) == "TRUE_EXPIRED")
                    {
                        if (MessageActivate.DialogQuestion("หมายเลขทะเบียนนี้หมดอายุแล้ว คุณต้องการที่จะใส่หมายเลขทะเบียนอีกครั้งหรือไม่?")
                                        == System.Windows.Forms.DialogResult.Yes)
                        {
                            this.txtActivateKeys.Focus();
                            this.txtActivateKeys.SelectAll();
                            return;
                        }
                        else
                        {
                            Application.Exit();
                        }
                    }
                    else if (Executing.Instance.ValidationLicence(this.txtActivateKeys.Text.Trim()) == "FALSE")
                    {
                        if (MessageActivate.DialogQuestion("หมายเลขลงทะเบียนไม่ถูกต้อง คุณต้องการที่จะใส่หมายเลขทะเบียนอีกครั้งหรือไม่?")
                                        == System.Windows.Forms.DialogResult.Yes)
                        {
                            this.txtActivateKeys.Focus();
                            this.txtActivateKeys.SelectAll();
                            return;
                        }
                        else
                        {
                            Application.Exit();
                        }
                    }
                }
                catch (Exception)
                {
                    if (MessageActivate.DialogQuestion("หมายเลขลงทะเบียนไม่ถูกต้อง คุณต้องการที่จะใส่หมายเลขทะเบียนอีกครั้งหรือไม่?")
                            == System.Windows.Forms.DialogResult.Yes)
                    {
                        this.txtActivateKeys.Focus();
                        this.txtActivateKeys.SelectAll();
                        return;
                    }
                    else
                    {
                        Application.Exit();
                    }
                }
                finally
                {
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        #endregion

        #region Event

        private void frmActivateLicense_Load(object sender, EventArgs e)
        {
            this.txtActivateKeys.Focus();
            this.txtActivateKeys.SelectAll();
        }
        private void txtActivateKeys_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SubmitLicence();
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            SubmitLicence();
        }

        private void lklLogout_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion
    }
}